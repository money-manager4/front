// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {

  apiKey: "AIzaSyC9M68NX8oJ2d3tuM6MfCw3S8re1uiS1rk",

  authDomain: "moneymanager-19.firebaseapp.com",

  projectId: "moneymanager-19",

  storageBucket: "moneymanager-19.appspot.com",

  messagingSenderId: "77313572454",

  appId: "1:77313572454:web:24410aa858c5b9747237a7",

  measurementId: "G-C6VCT7W0J7"

}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
