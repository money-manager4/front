import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Gasto, User } from './models/models';
import { Observable } from 'rxjs';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class FireserviceService {

  constructor(
    public fireservices: AngularFirestore,
    public fireauth: AngularFireAuth,
    private alertController: AlertController,
  ) {

  }

  loginWithEmail(correo: string, password: string) {
    return this.fireauth.signInWithEmailAndPassword(correo, password);
  }
  signupUser(data: User) {
    return this.fireauth.createUserWithEmailAndPassword(data.email, data.password);
  }

  saveDetails(data: any) {
    return this.fireservices.collection('users').doc(data.uid).set(data);
  }

  createDoc(data: any, path: string, id: string) {
    const collection = this.fireservices.collection(path);
    return collection.doc(id).set(data);
  }
  getDoc<User>(path: string, id: string) {
    const collection = this.fireservices.collection<User>(path);
    return collection.doc(id).valueChanges();
  }

  singout() {
    return this.fireauth.signOut();
  }
  updateDoc<T>(path: string, id: string, data: T): Promise<void> {
    const documentRef: AngularFirestoreDocument<T> = this.fireservices.collection(path).doc(id);
    return documentRef.update(data);
  }
  stateAuth() {
    return this.fireauth.authState;
  }

  getExpense<Gasto>(userId: string, gastoId: string) {
    const collection = this.fireservices.collection<Gasto>('users').doc(userId).collection('gasto');
    return collection.doc(gastoId).valueChanges();
  }
  getGasto(userId: string): Observable<any> {
    return this.fireservices
      .collection<Gasto>('users')
      .doc(userId)
      .collection('gasto')
      .valueChanges();
  }

  async addGasto(userId: string, expense: Gasto): Promise<void> {
    try {
      await this.fireservices
        .collection<Gasto>('users')
        .doc(userId)
        .collection('gasto')
        .add(expense);
      return console.log('gasto creado');
    } catch (error) {
      return console.error('Error adding expense:', error);
    }
  }
  async resetPassword(email: string) {
    try {
      await this.fireauth.sendPasswordResetEmail(email);
      const alert = await this.alertController.create({
        header: 'Buen trabajo',
        message: 'Se ha enviado un correo electrónico de restablecimiento de contraseña.',
        buttons: ['Aceptar']
      });
      await alert.present();
    } catch (error) {
      const alert = await this.alertController.create({
        header: 'Error',
        message: 'No se pudo enviar el correo electrónico de restablecimiento.',
        buttons: ['Aceptar']
      });
      await alert.present();
    }
  }


}
