import { Component, OnInit } from '@angular/core';
import { FireserviceService } from '../fireservice.service';
import { User } from '../models/models';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  data:User= {
    name: '',
    email: '',
    password: '',
    uid: ''
  };

  constructor(
    public auth:FireserviceService,
    private alertController: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
  }
  back(){
    window.history.back();
  }  

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Buen trabajo',
      message: message,
      buttons: [      {
        text: 'Aceptar',
        cssClass: 'customClass'
      }],
    });
    await alert.present();
  }

  async presentErrorAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      message: message,
      buttons: [      {
        text: 'Aceptar',
        cssClass: 'errorClass'
      }],
    });
    await alert.present();
  }

  async signup() {
    const res = await this.auth.signupUser(this.data).catch(err => {
      this.presentErrorAlert(err.message);
    });
    if (res) {
      const path = 'users';
      const id = res?.user?.uid || '';
      this.data.uid = id;
      this.data.password = '';
      await this.auth.createDoc(this.data, path, id);
      this.presentAlert("Tu cuenta ha sido creada con éxito, ya puedes iniciar sesión desde la pantalla de inicio");
      this.router.navigate(['/login']);
    }
  }
}