import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { FireserviceService } from '../fireservice.service';
import { Gasto, User } from '../models/models';
import { AlertButton, AlertController, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  login: boolean = false;
  nombre: string = '';
  gastoCollection!: AngularFirestoreCollection<Gasto>;
  gasto!: Observable<Gasto[]>;
  id: string='';
  gastos: any[] = [];
  disponible: number = 0;
  uid: string = '';

  public alertButtons: AlertButton[]= [{
    text: 'Cancelar',
    cssClass: 'cancelClass',
    role: 'cancel',
  },
  {
    text: 'Aceptar',
    cssClass: 'customClass',
    handler: (value) => {
      this.agregarGasto(value);
    }
  }
];
public alertInputs = [
  {
    name: 'categoria',
    type: 'text',
    placeholder: 'Ingresa la categoría',
    value: '',
  },
  {
    name: 'nombreGasto',
    type: 'text',
    placeholder: 'Ingresa la descripción del gasto',
    value: '',
  },
  {
    name: 'monto',
    type: 'number',
    placeholder: 'Ingresa el monto',
  },
];

  constructor(private auth:FireserviceService,
    private router:Router,
    private firestore: AngularFirestore,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastController: ToastController) {
      this.getUid();
      this.auth.stateAuth().subscribe(user => {
        if (user) {
          this.id = user.uid;
        }
      });
    }

  ngOnInit() {
  }

  logout() {
    this.auth.singout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  async eliminarGasto(gastoId: string) {
    const confirmAlert = await this.alertController.create({
      header: 'Eliminar gasto',
      message: '¿Estás seguro de que quieres eliminar este gasto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Eliminar',
          handler: () => {
            // Buscar el índice del gasto en el arreglo
            const index = this.gastos.findIndex((gasto) => gasto.id === gastoId);
            if (index > -1) {
              // Eliminar el gasto del arreglo
              this.gastos.splice(index, 1);
               this.actualizarDisponible();
            }
            // Actualizar la colección de gastos en la base de datos
            this.gastoCollection.doc(gastoId).delete();
            this.actualizarDisponible();
          },
        },
      ],
    });
    await confirmAlert.present();
  }

  actualizarDisponible() {
    this.gastoCollection.valueChanges().subscribe((gastos) => {
      let totalGastos = 0;
      gastos.forEach((gasto) => {
        totalGastos += gasto.monto;
      });

      this.firestore.collection(`users/${this.id}/ingreso`).valueChanges().subscribe((ingresos: any[]) => {
        let totalIngresos = 0;
        ingresos.forEach((ingreso) => {
          totalIngresos += ingreso.monto;
        });

        this.disponible = totalIngresos - totalGastos;
      });
    });
  }

  async showToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'bottom',
    });
    toast.present();
  }

  getDataUser(uid: string){
    const path = 'users';
    const id = uid;
    this.auth.getDoc<User>(path, id).subscribe(res => {
      if (res) {
        this.nombre = res.name;
      }
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  getUid() {
    this.auth.stateAuth().subscribe(res => {
      if (res == null) {
        this.router.navigate(['/login']);
        this.login = false;
      } else {
        this.login = true;
        this.getDataUser(res.uid);
        const userId = res.uid;

        // Almacena el valor de res.uid en una variable local
        const uid = res.uid;

        this.firestore.collection('users').doc(userId).collection('gasto');
        this.gastoCollection = this.firestore.collection(`users/${userId}/gasto`);

        this.gastoCollection.snapshotChanges().subscribe((gastoSnapshots) => {
          this.gastos = gastoSnapshots.map((snapshot) => {
            const gasto = snapshot.payload.doc.data();
            const id = snapshot.payload.doc.id;
            return { id, ...gasto };
          });
        });
        this.actualizarDisponible();
        // Utiliza el valor de uid en el método agregarGasto
        this.uid = uid;
      }
    });
  }
  async editarGasto(gasto: any) {
    const editarAlert = await this.alertController.create({
      header: 'Editar gasto',
      message: 'Actualiza los datos del gasto',
      animated: true,
      cssClass: 'alert-custom',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'cancel-button',
        },
        {
          text: 'Guardar',
          cssClass: 'save-button',
          handler: (value) => {
            this.actualizarGasto(gasto.id, value);
          },
        },
      ],
      inputs: [
        {
          name: 'categoria',
          type: 'text',
          placeholder: 'Categoría',
          value: gasto.categoria,
        },
        {
          name: 'nombreGasto',
          type: 'text',
          placeholder: 'Descripción del gasto',
          value: gasto.nombreGasto,
        },
        {
          name: 'monto',
          type: 'number',
          placeholder: 'Monto',
          value: gasto.monto.toString(),
        },
      ],
    });
    await editarAlert.present();
  }

  actualizarGasto(gastoId: string, updatedData: any) {
    this.gastoCollection.doc(gastoId).update(updatedData);
  }


  agregarGasto(value: any) {
    if (value.categoria && value.nombreGasto && value.monto) {
      const nuevoGasto: Gasto = {
        categoria: value.categoria,
        nombreGasto: value.nombreGasto,
        monto: value.monto,
        userId: this.uid,
      };
      this.gastoCollection.add(nuevoGasto);
      this.alertController.dismiss();
    }
  }
}
