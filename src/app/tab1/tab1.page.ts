import { Component } from '@angular/core';
import { FireserviceService } from '../fireservice.service';
import { Router } from '@angular/router';
import { User } from '../models/models';
import { AlertController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable, combineLatest, forkJoin, map, of } from 'rxjs';
import { CurrencyPipe } from '@angular/common';
import * as firebase from 'firebase/compat';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  login: boolean = false;
  nombre: string = '';
  ingresoCollection!: AngularFirestoreCollection<any>;
  ingreso!: Observable<any[]>;
  gastoCollection!: AngularFirestoreCollection<any>;
  gasto!: Observable<any[]>;
  disponible: number = 0;
  gastosPorCategoria: { categoria: string; montoTotal: number }[] = [];
  ingresosPorCategoria: { categoria: string; montoTotal: number }[] = [];

  constructor(
    private auth: FireserviceService,
    private router: Router,
    private alertController: AlertController,
    private firestore: AngularFirestore,
    private currencyPipe: CurrencyPipe
  ) {
    this.auth.stateAuth().subscribe(res => {
      if (res) {
        this.login = true;
        this.getDataUser(res.uid);
        this.getMontos(res.uid);
      } else {
        this.router.navigate(['/login']);
        this.login = false;
      }
    });
  }

  logout() {
    this.auth.singout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getDataUser(uid: string) {
    const path = 'users';
    const id = uid;
    this.auth.getDoc<User>(path, id).subscribe(res => {
      if (res) {
        this.nombre = res.name;
      }
    });
  }

  async presentSaccefullAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Bienvenido a Money Manager',
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          cssClass: 'customClass'
        }
      ],
    });
    await alert.present();
  }

  getMontos(uid: string) {
    this.ingresoCollection = this.firestore.collection(`users/${uid}/ingreso`);
    this.ingreso = this.ingresoCollection.valueChanges();

    this.gastoCollection = this.firestore.collection(`users/${uid}/gasto`);
    this.gasto = this.gastoCollection.valueChanges();

    combineLatest([this.ingreso, this.gasto]).subscribe(([ingresos, gastos]) => {
      const totalIngresos = ingresos.reduce((acc, ingreso) => acc + Number(ingreso.monto), 0);
      const totalGastos = gastos.reduce((acc, gasto) => acc + Number(gasto.monto), 0);
      this.disponible = totalIngresos - totalGastos;

      const gastosAgrupados = gastos.reduce((acc: { [categoria: string]: number }, gasto: any) => {
        const categoria = gasto.categoria;
        const monto = Number(gasto.monto);
        acc[categoria] = (acc[categoria] || 0) + monto;
        return acc;
      }, {});
  
      const ingresosAgrupados = ingresos.reduce((acc: { [categoria: string]: number }, ingreso: any) => {
        const categoria = ingreso.categoria;
        const monto = Number(ingreso.monto);
        acc[categoria] = (acc[categoria] || 0) + monto;
        return acc;
      }, {});
  
      // Convertir los objetos en arreglos de categorías con montos totales
      this.gastosPorCategoria = Object.keys(gastosAgrupados)
        .map((categoria) => ({
          categoria,
          montoTotal: gastosAgrupados[categoria],
        }))
        .sort((a, b) => b.montoTotal - a.montoTotal) // Ordenar por mayor monto
        .slice(0, 2); // Obtener los 3 primeros elementos
  
      this.ingresosPorCategoria = Object.keys(ingresosAgrupados)
        .map((categoria) => ({
          categoria,
          montoTotal: ingresosAgrupados[categoria],
        }))
        .sort((a, b) => b.montoTotal - a.montoTotal) // Ordenar por mayor monto
        .slice(0, 2); // Obtener los 3 primeros elementos
    });
      
  }

  calcularDisponible() {
    const formattedDisponible = this.currencyPipe.transform(this.disponible, 'USD', 'symbol', '1.0-0');
    return formattedDisponible;
  }

}

