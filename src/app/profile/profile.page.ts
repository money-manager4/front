import { Component, OnInit } from '@angular/core';
import { FireserviceService } from '../fireservice.service';
import { User } from '../models/models';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  correo: string = '';
  nombre: string = '';


  constructor(private auth: FireserviceService) {}

  ngOnInit() {
    this.getUid();
  }

  getDataUser(uid: string) {
    const path = 'users';
    const id = uid;
    this.auth.getDoc<User>(path, id).subscribe((res) => {
      if (res) {
        this.nombre = res.name;
        this.correo = res.email;
      }
    });
  }

  getUid() {
    this.auth.stateAuth().subscribe((res) => {
      if (res) {
        this.getDataUser(res.uid);
      }
    });
  }
}
