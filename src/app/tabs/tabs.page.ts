import { Component, EventEmitter, Output } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { MenuToggleService } from '../menu-toggle.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  @Output() menuToggle = new EventEmitter<void>();

  constructor(private menuToggleService: MenuToggleService, private menuController:MenuController) {}

  openMenu() {
    this.menuToggleService.toggleMenu();
  }
}
