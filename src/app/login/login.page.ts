import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FireserviceService } from '../fireservice.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentials = {
    email: "",
    password: ""
  };

  constructor(
    public router:Router,
    public fireService:FireserviceService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }
  async presentErrorAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      message: message,
      buttons: [      {
        text: 'Aceptar',
        cssClass: 'customClass'
      }],
    });
    await alert.present();
  }
  async presentSaccefullAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Bienvenido a Money Manager',
      message: message,
      buttons: [      {
        text: 'Aceptar',
        cssClass: 'customClass'
      }],
    });
    await alert.present();
  }

  async login() {
    const res = await this.fireService.loginWithEmail(this.credentials.email, this.credentials.password).catch(
      err => this.presentErrorAlert("Correo o contraseña incorrectos"));
    if (res) {
      this.presentSaccefullAlert(this.credentials.email);
      this.router.navigate(['/tabs/tab1']);
    }
  }
  signup(){
    this.router.navigateByUrl('signup');
  }
  async forgotPassword() {
    const alert = await this.alertController.create({
      header: 'Bienvenido a Money Manager',
      message: 'Ingresa tu correo electrónico para recuperar tu contraseña',
      inputs: [  {
        name: 'correo',
        type: 'text',
        placeholder: 'Ingresa tu correo electrónico',
        value: '',
      },
      ],
      buttons: [   
        {
        text: 'Cancelar',
        },   
        {
        text: 'Aceptar',
        handler: (value) => {
          this.fireService.resetPassword(value.correo);
        }
      }],
    });
    await alert.present();
  }
  
}