export interface User{
    name: string;
    email: string;
    password: string;
    uid: string;
}

export interface Gasto {
    id?: string; // Identificador del gasto (opcional)
    userId: string; // Identificador del usuario asociado al gasto
    categoria: string; // Categoría del gasto
    nombreGasto: string; // Nombre del gasto
    monto: number; // Cantidad del gasto
  }
