import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuToggleService {
  private menuToggleSubject = new BehaviorSubject<boolean>(false);
  menuToggle$ = this.menuToggleSubject.asObservable();

  toggleMenu() {
    this.menuToggleSubject.next(this.menuToggleSubject.value);
  }
}
