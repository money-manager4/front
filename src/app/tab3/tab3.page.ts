import { Component } from '@angular/core';
import { FireserviceService } from '../fireservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/models';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { AlertButton, AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  login : boolean = false;
  nombre : string = '';
  ingresoCollection!: AngularFirestoreCollection<any>;
  id: string='';
  ingresos: any[] = [];
  disponible: number = 0;

  public alertButtons: AlertButton[]= [{
    text: 'Cancelar',
    cssClass: 'cancelClass',
    role: 'cancel',
  },
  {
    text: 'Aceptar',
    cssClass: 'customClass',
    handler: (value) => {
      this.agregarIngreso(value);
    }
  }
];
public alertInputs = [
  {
    name: 'categoria',
    type: 'text',
    placeholder: 'Ingresa la categoría',
    value: '',
  },
  {
    name: 'nombreIngreso',
    type: 'text',
    placeholder: 'Ingresa la descripción del ingreso',
    value: '',
  },
  {
    name: 'monto',
    type: 'number',
    placeholder: 'Ingresa el monto',
  },
];

  constructor(private auth:FireserviceService,
    private router:Router,
    private firestore: AngularFirestore,
    private alertController: AlertController,
    private navCtrl: NavController,
    private route: ActivatedRoute) {


      this.getUid();
      this.auth.stateAuth().subscribe(user => {
        if (user) {
          this.id = user.uid;
        }
      });
    }
  ngOnInit() {}

  getUid() {
    this.auth.stateAuth().subscribe(res => {
      if (res == null) {
        this.router.navigate(['/login']);
        this.login = false;
      } else {
        this.login = true;
        this.getDataUser(res.uid);
        const userId = res.uid;
        this.firestore.collection('users').doc(userId).collection('ingreso')
        // Accede a la colección de ingresos del usuario
        this.ingresoCollection = this.firestore.collection(`users/${userId}/ingreso`);

        this.ingresoCollection.snapshotChanges().subscribe((ingresoSnapshots) => {
          this.ingresos = ingresoSnapshots.map((snapshot) => {
            const ingreso = snapshot.payload.doc.data();
            const id = snapshot.payload.doc.id;
            return { id, ...ingreso };
          });
        });
        this.actualizarDisponible();
      }
    });
  }
  logout() {
    this.auth.singout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  async eliminarIngreso(ingresoId: string) {
    const confirmAlert = await this.alertController.create({
      header: 'Eliminar ingreso',
      message: '¿Estás seguro de que quieres eliminar este ingreso?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'cancel-button',
        },
        {
          text: 'Eliminar',
          cssClass: 'delete-button',
          handler: () => {
            // Buscar el índice del gasto en el arreglo
            const index = this.ingresos.findIndex((ingreso) => ingreso.id === ingresoId);
            if (index > -1) {
              // Eliminar el gasto del arreglo
              this.ingresos.splice(index, 1);
               this.actualizarDisponible();
            }
            // Actualizar la colección de ingreso en la base de datos
            this.ingresoCollection.doc(ingresoId).delete();
            this.actualizarDisponible();
          },
        },
      ],
    });
    await confirmAlert.present();
  }
  editarIngreso(ingreso: any) {
    this.alertController
      .create({
        header: 'Editar ingreso',
        message: 'Actualiza los datos del ingreso',
        animated: true,
        cssClass: 'alert-custom',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'cancel-button',
          },
          {
            text: 'Guardar',
            cssClass: 'save-button',
            handler: (value) => {
              this.actualizarIngreso(ingreso.id, value);
            },
          },
        ],
        inputs: [
          {
            name: 'categoria',
            type: 'text',
            placeholder: 'Categoría',
            value: ingreso.categoria,
          },
          {
            name: 'nombreIngreso',
            type: 'text',
            placeholder: 'Descripción del ingreso',
            value: ingreso.nombreIngreso,
          },
          {
            name: 'monto',
            type: 'number',
            placeholder: 'Monto',
            value: ingreso.monto.toString(),
          },
        ],
      })
      .then((editarAlert) => {
        editarAlert.present();
      });
  }
  actualizarIngreso(ingresoId: string, updatedData: any) {
    this.ingresoCollection.doc(ingresoId).update(updatedData);
  }

  actualizarDisponible() {
    this.firestore.collection(`users/${this.id}/gasto`).valueChanges().subscribe((gastos: any[]) => {
      let totalGastos = 0;
      gastos.forEach((gasto) => {
        totalGastos += gasto.monto;
      });

      this.ingresoCollection.valueChanges().subscribe((ingresos) => {
        let totalIngresos = 0;
        ingresos.forEach((ingreso) => {
          totalIngresos += ingreso.monto;
        });

        this.disponible = totalIngresos - totalGastos;
      });
    });
  }
  getDataUser(uid: string){
    const path = 'users';
    const id = uid;
    this.auth.getDoc<User>(path, id).subscribe(res => {
      if (res) {
        this.nombre = res.name;
      }
    });
  }


  agregarIngreso(value: any) {
    if (value.categoria && value.nombreIngreso && value.monto) {
      const nuevoIngreso = {
        categoria: value.categoria,
        nombreIngreso: value.nombreIngreso,
        monto: value.monto
      };
      this.ingresoCollection.add(nuevoIngreso);
      this.alertController.dismiss();
    }
  }

}
