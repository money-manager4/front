import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { MenuToggleService } from '../menu-toggle.service';
import { FireserviceService } from '../fireservice.service';
import { User } from '../models/models';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent  implements OnInit {

  menuItems = [
    { title: 'Inicio', url: '/tabs/tab1', icon: 'home-outline', isHome: true },
    { title: 'Perfil', url: '/profile', icon: 'person-outline' },
    { title: 'Configuración', url: '/settings', icon: 'settings-outline' },
    // Agrega más opciones de menú según tus necesidades...
  ];

  closeMenu(url: string) {
    this.menuController.toggle();
    if (url === '/profile') {
      this.router.navigateByUrl(url);
    }
  }

  id: string='';
  constructor(private menuToggleService: MenuToggleService,
    private menuController: MenuController,
    private router: Router,
    private auth:FireserviceService)
    {
      this.menuToggleService.menuToggle$.subscribe(() => {
        this.menuController.toggle();
      });
      this.getUid();

    }

  ngOnInit() {

  }
  nombre: string = '';
  correo: string = '';
  imagenUrl = '';
  getDataUser(uid: string){

    const path = 'users';
    const id = uid;
    this.auth.getDoc<User>(path, id).subscribe(res => {
      if (res) {
        this.nombre = res.name;
        this.correo = res.email;
      }
    });
  }
  getUid() {
    this.auth.stateAuth().subscribe(res => {
      if (res) {
        this.getDataUser(res.uid);
      }
    });
  }

}
